
var moduleToDownload = null;

if(document.getElementById("id_search_button_classes") != null){
    document.getElementById("id_search_button_classes").addEventListener("click", function(){
        setModulesClassesInHiddenFields();
    });
}

$(document).ready(function(e){
    try{
        $('#downloadFilesSelect').msDropDown();
    } catch(err){
        alert(err.message);
    }
});

if(document.getElementById("downloadFilesSelect") != null){


    moduleToDownload = $("#downloadFilesSelect option:selected").text();


    $("#downloadFilesSelect").change(function(el){
        moduleToDownload = $("#downloadFilesSelect option:selected").text();
    });
}

/**
 * Download module file
 */
function downloadModuleFile(){
    setBackgroundOfDownloadedModule(moduleToDownload);
    window.location = document.getElementById("downloadFilesSelect").value;
}

/**
 * Change the background of downloaded modules in the module list
 * @param {type} moduleName
 */
function setBackgroundOfDownloadedModule(moduleName){
    $("#downloadFilesSelect_child li .ddlabel:contains('" + moduleName+  "')").each(function(){
        if($(this)[0].textContent == moduleName){        
            $(this).parent().css("background-color", "#4CD964");
        }
    });
}

/**
 * Set modules classes values in each hidden fields
 */
function setModulesClassesInHiddenFields(){
    for(var m = 0 ; m < modules.length ; m++){

        var form_module = document.getElementById("form_module_" + modules[m]);

        var valueToSet = [];

        /**
         * Get all value for each module
         */
        for(var d = 0 ; d < form_module.childNodes.length ; d++){
            var rule_higher_ofValue = form_module.childNodes[d].getElementsByClassName("rule_higher_of")[0].value;
            var rule_lowest_ofValue = form_module.childNodes[d].getElementsByClassName("rule_lowest_of")[0].value;

            valueToSet.push({"higher_of": rule_higher_ofValue, "lowest_of": rule_lowest_ofValue});
        }



        document.getElementsByName("module_" + modules[m])[0].value = JSON.stringify({"key": modules[m], "value": valueToSet});

    }
}

/**
 * Deletes the last child class of a module
 * @param {type} moduleid
 */
function deleteLastChild(moduleid){
    var form = document.getElementById("form_module_" + moduleid);
    form.lastChild.remove();
}

/**
 * Create a div DOM element with a classname
 * @param {type} classname
 * @returns {createElementDiv.div|Element}
 */
function createElementDiv(classname){
    var div = document.createElement("div");
    div.setAttribute("class", classname);
    return div;
}

/**
 * Add class to module
 */
function addClassModule(moduleid){

    var form = document.getElementById("form_module_" + moduleid);

    /**
     * Create inner elements
     */
    var module_class = createElementDiv("module_class");

    var name_form = createElementDiv("name_form");
    name_form.innerHTML = "Class " + (form.childNodes.length+1);

    var rule_name1 = createElementDiv("rule_name");
    rule_name1.innerHTML = "must be ≥";

    var rule_input1 = createElementDiv("rule_input");
    var rule_higher = document.createElement("input");
    rule_higher.setAttribute("type", "number");
    rule_higher.setAttribute("class", "rule_higher_of");
    rule_higher.setAttribute("value", "0");

    var rule_name2 = createElementDiv("rule_name");
    rule_name2.innerHTML = "must be <=";

    var rule_input2 = createElementDiv("rule_input");
    var rule_lower = document.createElement("input");
    rule_lower.setAttribute("type", "number");
    rule_lower.setAttribute("class", "rule_lowest_of");
    rule_lower.setAttribute("value", "0");

    var deleteButton = createElementDiv("deleteButton");
    deleteButton.innerHTML = "delete";
    deleteButton.setAttribute("onclick", "deleteLastChild(" + moduleid + ")")

    /**
     * Append elements
     */
    rule_input1.appendChild(rule_higher);
    rule_input2.appendChild(rule_lower);

    module_class.appendChild(name_form);
    module_class.appendChild(rule_name1);
    module_class.appendChild(rule_input1);
    module_class.appendChild(rule_name2);
    module_class.appendChild(rule_input2);

    module_class.appendChild(deleteButton);

    form.appendChild(module_class);

}