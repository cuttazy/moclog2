
/**
 * 
 * CATEGORIES PAGE
 * 
 */


/**
* Event called on check or uncheck a checkbox of categories
*/
function onClickCheckBox(obj){
   if (obj.checked)
   {
       addElementToCategoriesToShow(obj.getAttribute("paramid"));
   }
   else
   {
       removeCategorieToShow(obj.getAttribute("paramid"));
   }
   paramToCheckBox(obj.checked, obj.getAttribute("paramid"), obj.getAttribute("parampath"));
}

/**
* Add id in categories to show
*/
function addElementToCategoriesToShow(id){
   var cat_to_show = document.getElementsByName("categories_to_show")[0].value;
   cat_to_show = JSON.parse(cat_to_show);
   cat_to_show.push(id);
   document.getElementsByName("categories_to_show")[0].value =  JSON.stringify(cat_to_show);
}

/**
 * Select all categories
 * @returns {undefined}
 */
function selectAllCategories(){
    var els = document.getElementsByClassName("mclg2_box");
    for(var i = 0 ; i < els.length ; i++){
        
        /**
         * Get parampath attribute, and check if the first layer
         */
        var parampath = els[i].getAttribute("parampath");
        var currentLayer = (parampath.match(new RegExp("/", "g")) || []).length;
        if(currentLayer == 1){
            els[i].checked = false;
            els[i].click();
        }
        
    }
}

/**
* Enable or disable checkbox on select parent in categories list
*/
function paramToCheckBox(addParam, paramid, path){
   var els = document.getElementsByClassName("mclg2_box");
   for(var i = 0 ; i < els.length ; i++){
       var pathEl = els[i].getAttribute("parampath");
       pathEl = pathEl.substring(0, path.length+1);

       // Check path and check that the element is not the same element
       if(pathEl == path + "/" && paramid != els[i].getAttribute("paramid")){
           // Check if add or remove attribute
           if(addParam){
               els[i].setAttribute("disabled", "true");
               els[i].checked = false;
               removeCategorieToShow(els[i].getAttribute("paramid"));

               /**
                * Hide div of checkbox
                */
               var divToHide = document.getElementById("checkbox_parent_" + els[i].getAttribute("paramid"));
               divToHide.setAttribute("style", "display: none;");

           }
           else{
               els[i].removeAttribute("disabled");

               /**
                * Show div of checkbox
                */
               var divToHide = document.getElementById("checkbox_parent_" + els[i].getAttribute("paramid"));
               divToHide.removeAttribute("style");
           }
       }

   }
}

/**
* Remove an id of categories's array
*/
function removeCategorieToShow(id){
   var cat_to_show = document.getElementsByName("categories_to_show")[0].value;
   cat_to_show = JSON.parse(cat_to_show);
   var new_arr = [];
   for(var i = 0 ; i < cat_to_show.length ; i++){
       if(cat_to_show[i] != id){
           new_arr.push(cat_to_show[i]);
       }
   }
   document.getElementsByName("categories_to_show")[0].value = JSON.stringify(new_arr);
}

$('legend').click(function(el) {
  $(el.currentTarget.parentElement).toggleClass('active');
});