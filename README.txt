===================================================================================
                                  M O C L O G 2
                    
                          Version 1.0 - April 25, 2016 
		
		Università della Svizzera italiana, Lugano, Switzerland
                    
                                       README
===================================================================================

0)  Requirements

    MOCLog2 is developed on the LAMP platform (Linux, Apache, MySQL and PHP), 
    we didn't test it in Windows environments.

    The requirements for Gismo are:
    - PHP v. 5.4.4
    - MySQL 5.5.31
    - Moodle 2.9.x

1)  Setup
    
    a) REMOVE OLD VERSION

    Please remove any old version of the plugin.

    b) INSTALL FILES    
    
    Please follow the standard procedure to install Moodle plugins:
    - copy the MOCLog2 folder into the moodle reports directory (MOODLE_DOCUMENT_ROOT/report)
    - logon to moodle as administrator and visit the Notifications page
    - configure the new settings page

    With git:
        git clone https://cuttazy@bitbucket.org/cuttazy/moclog2.git moclog2

    c) LAUNCH DATA EXPORTER FOR THE FIRST TIME
    
    MOCLog2 needs to perform some computation on regular basis. The purpose of this 
    computation is to extract data from logs and derive aggregated information. 
    
    This computation is done by a scheduled task, by default it runs at 3am (server time)
    but can be changed in Site Administration -> Server -> Scheduled tasks -> MOCLog2 data exporter task

    When you install  MOCLog2 report and you don't want to wait the next day to see the result,
    you can run the task manually (ONLY FROM CLI):
    php admin/tool/task/cli/schedule_task.php --execute=\\report_moclog2\\task\\moclog2_cron_collectdata

2)  Configuration

    MOCLog2 doesn't need any specific configuration. Everything runs out of the box.

3)  Use
    
    Please access your reports as and administratior from Site administration - Reports - MOCLog 2

4)  Further information
    
    Please visit http://moclog.ch/ for further information
    
5)  MOCLog2 is released under GPL version 3 License
    
    
The eLab Team
    
===================================================================================
