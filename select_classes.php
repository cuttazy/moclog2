<?php

/**
 * MOCLog2 report
 *
 * @package    report_moclog2
 * @copyright  eLab Yann Cuttaz, Christian Milani, Riccardo Mazza
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require(dirname(__FILE__) . '/../../config.php');
require_once($CFG->libdir . '/adminlib.php');
admin_externalpage_setup('report_moclog2', '', null, '', array('pagelayout' => 'report'));

$PAGE->requires->jquery();
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/report/moclog2/lib/ms-Dropdown/js/msdropdown/jquery.dd.min.js'));
$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/report/moclog2/lib/ms-Dropdown/css/msdropdown/dd.css'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/report/moclog2/lib/moclog2/scripts/select_classes.js'));
$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/report/moclog2/lib/moclog2/style/moclog2.css'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/report/moclog2/lib/moclog2/scripts/moclog2.js'));

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('pluginname', 'report_moclog2'));

$form = new \report_moclog2\ClassesForm();

if ($form->is_cancelled()) {
    
} else if ($dataForm = $form->get_data()) {

    if(isset($dataForm->search_button_classes)){
        /**
         * Load params object
         */
        $params = \report_moclog2\MocLog2Params::Load();

        $params->classes = array();

        foreach ($dataForm as $module_classes_encoded) {

            $module_classes = json_decode($module_classes_encoded);

            if (is_object($module_classes) && $module_classes->key != null)
                $params->addClasses($module_classes->key, $module_classes->value);
        }

        $params->modules_activities = report_moclog2\ModulesManagement::getModulesTypes();

        $params->Save();

        
        echo \report_moclog2\Progression::getCurrentProgression(4);
        

        /**
         * General reports
         */
        if ($params->general_report) {
            echo "<div>" . get_string("generalreportsdescription", "report_moclog2") . "</div>";

            echo $OUTPUT->single_button(new moodle_url('/report/moclog2/download_report_excel.php'), "Download reports");
        }

        /**
         * Specific reports
         */
        echo get_string("modulesreportsdescription", "report_moclog2") . "<br>";

        /**
         * Modules selection
         */
        echo '<select id="downloadFilesSelect">';
        foreach ($params->modules_to_show as $moduleid) {
            $modulename = report_moclog2\ModulesManagement::getModuleNameById($moduleid);
            echo '<option id="' . $modulename . '" value="' . new moodle_url('/report/moclog2/download_modules_report_excel.php?moduleid=' . $moduleid) . '">' . get_string('pluginname', 'mod_' . $modulename) . '</option>';
        }
        echo '</select>';

        /**
         * Download button
         */
        echo '<input type="button" onclick="downloadModuleFile()" value="Download">';
    } else{
        redirect("select_params.php");
    }
} else {
    $params = \report_moclog2\MocLog2Params::Load(false);

    /**
     * Prepare data and show form
     */
    $modules = $params->modules_to_show;
    echo '<div class="classes_page">';

    /**
     * Print the modules in javascript section
     */
    echo '<script>var modules = ' . json_encode($modules) . ';</script>';
    
    echo \report_moclog2\Progression::getCurrentProgression(3);
    
    echo get_string('classespreferencesdescription', 'report_moclog2') . "<br>";

    
    echo '</div>';
    
    $form->display();
}
echo $OUTPUT->footer();