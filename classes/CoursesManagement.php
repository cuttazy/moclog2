<?php

/**
 * MOCLog2 report
 *
 * @package    report_moclog2
 * @copyright  eLab Yann Cuttaz, Christian Milani, Riccardo Mazza
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace report_moclog2;

class CoursesManagement {

    /**
     * Return the list of courses with module count
     * @global \report_moclog2\type $DB
     * @param type $courses: Array of courses id
     * @param type $moduleid_required: The module id required
     * @return \stdClass
     */
    public static function getModulesFromCoursesList($courses, $moduleid_required) {
        global $DB;

        /**
         * Get each id
         */
        $queryParam = array();
        foreach ($courses as $course) {
            $queryParam[] = $course->id_course;
        }

        $query = "SELECT course, module, count(*) as count_module FROM `{course_modules}` WHERE module = " . $moduleid_required . " AND course IN (" . implode(',', $queryParam) . ") group by module, course ORDER BY course ASC";
        $result = $DB->get_records_sql($query);

        /**
         * Fill the list
         */
        $ret = array();
        foreach ($courses as $course) {
            $course_module = new \stdClass();
            $course_module->course = $course->id_course;
            $course_module->module = $moduleid_required;
            $course_module->count_module = isset($result[$course->id_course]) ? $result[$course->id_course]->count_module : 0;
            $ret[$course->id_course] = $course_module;
        }

        unset($result);
        unset($queryParam);
        unset($courses);

        return $ret;
    }

    /**
     * Return a list of filtred courses
     * @global \report_moclog2\type $DB
     * @param type $categoryid
     * @param type $params
     * @return type
     */
    public static function getCoursesFromCategoryID($categoryid, $params) {
        global $DB;

        $hiddenCourses = ($params->hidden_courses) ? ")" : " AND visible = '1')";

        $query = "SELECT id_course FROM {report_moclog2_log}
                 WHERE id_course IN
                    (SELECT id_course FROM {report_moclog2_courses}
                    WHERE (category LIKE '%/" . $categoryid . "/%'
                    OR category LIKE '$categoryid/%'
                    OR category LIKE '%/$categoryid'
                    OR category = '$categoryid')
                    AND creation_date >= '$params->course_creation'
                    AND teachers_enrolled >= '" . $params->min_enrolled_teachers . "'
                    AND students_enrolled >= '" . $params->min_enrolled_students . "'" . $hiddenCourses . "
                GROUP BY id_course
                HAVING SUM(tot_student_access) + SUM(tot_teacher_access) >= '" . $params->logs_required . "'";

        return $DB->get_records_sql($query);
    }

    /**
     * Get the number of courses in a category
     * @global \report_moclog2\type $DB
     * @param type $categoryid: The category's id
     * @return type: Number of courses
     */
    public static function getTotCoursesFromCategoryId($categoryid) {

        global $DB;
        $totCourses = 0;
        $categoriesToCheck = CategoriesManagement::getSubCategories($categoryid);

        /**
         * For each (sub) categories, get the number of courses
         */
        foreach ($categoriesToCheck as $category) {
            $totToAdd = $DB->get_records_sql("SELECT count(*) 'value' FROM {course} WHERE category = ?", array($category->id));
            $totCourses += $totToAdd[key($totToAdd)]->value;
        }

        return $totCourses;
    }
    
    /**
     * Get the number of empty courses in a list of courses
     * @global \report_moclog2\type $DB
     * @return type: Number of empty courses
     */
    public static function getTotEmptyCourses($courses){
        global $DB;
        $totEmptyCourses = 0;
        
        foreach ($courses as $course) {
            $totForumQuery = $DB->get_records_sql("SELECT count(*) 'value' FROM {forum} WHERE course = ?", array($course));
            $totForum = $totForumQuery[key($totForumQuery)]->value;
            if($totForum == 1){ // If there is 1 forum, check other activities
                
                $query = "
                    SELECT sum(value) as 'tot'
                    FROM (
                        SELECT count(*) 'value' FROM {assignment} WHERE course = $course
                        
                        UNION ALL

                        SELECT count(*) 'value' FROM {chat} WHERE course = $course
                        
                        UNION ALL

                        SELECT count(*) 'value' FROM {choice} WHERE course = $course
                        
                        UNION ALL

                        SELECT count(*) 'value' FROM {data} WHERE course = $course
                        
                        UNION ALL

                        SELECT count(*) 'value' FROM {lti} WHERE course = $course
                        
                        UNION ALL

                        SELECT count(*) 'value' FROM {feedback} WHERE course = $course
                        
                        UNION ALL

                        SELECT count(*) 'value' FROM {glossary} WHERE course = $course
                        
                        UNION ALL

                        SELECT count(*) 'value' FROM {lesson} WHERE course = $course
                        
                        UNION ALL

                        SELECT count(*) 'value' FROM {quiz} WHERE course = $course
                        
                        UNION ALL

                        SELECT count(*) 'value' FROM {scorm} WHERE course = $course
                        
                        UNION ALL

                        SELECT count(*) 'value' FROM {wiki} WHERE course = $course
                        
                        UNION ALL

                        SELECT count(*) 'value' FROM {workshop} WHERE course = $course
                        
                        UNION ALL

                        SELECT count(*) 'value' FROM {book} WHERE course = $course
                        
                        UNION ALL

                        SELECT count(*) 'value' FROM {resource} WHERE course = $course
                        
                        UNION ALL

                        SELECT count(*) 'value' FROM {folder} WHERE course = $course
                        
                        UNION ALL

                        SELECT count(*) 'value' FROM {imscp} WHERE course = $course
                        
                        UNION ALL

                        SELECT count(*) 'value' FROM {label} WHERE course = $course
                        
                        UNION ALL

                        SELECT count(*) 'value' FROM {page} WHERE course = $course
                        
                        UNION ALL

                        SELECT count(*) 'value' FROM {url} WHERE course = $course

                    ) as t";
                
                $totOthersQuery = $DB->get_records_sql($query);
                $totOthers = $totOthersQuery[key($totOthersQuery)]->tot;
                if($totOthers == 0){ // If other activities = 0, this is an empty course
                    $totEmptyCourses++;
                }
                
            }
        }
        
        return $totEmptyCourses;
    }

    /**
     * Return the number of teachers enrolled in courses
     * @param type $courses
     * @return type
     */
    public static function getTotTeachersEnrolledFromCourses($courses) {
        return CoursesManagement::getTotUsersEnrolledFromCourses("teacher", $courses);
    }

    /**
     * Return the number of students enrolled in courses
     * @param type $courses
     * @return type
     */
    public static function getTotStudentsEnrolledFromCourses($courses) {
        return CoursesManagement::getTotUsersEnrolledFromCourses("student", $courses);
    }

    /**
     * Return the number of users enrolled in courses
     * @global \report_moclog2\type $DB
     * @param type $userRoleName: teacher or student
     * @param type $courses: Array of courses id
     * @return int: The number of students or teachers enrolled in courses
     */
    public static function getTotUsersEnrolledFromCourses($userRoleName, $courses) {
        global $DB;

        if (count($courses) > 0) {
            $totUsers = $DB->get_records_sql("SELECT SUM(" . $userRoleName . "s_enrolled) as 'tot_users' FROM {report_moclog2_courses} WHERE id_course IN (" . implode(',', $courses) . ")");
            return $totUsers[key($totUsers)]->tot_users;
        } else {
            return 0;
        }
    }

    /**
     * Return an array with key: module, value: number of courses of modules
     * @global \report_moclog2\type $DB
     * @param type $categories: List of categories
     * @param type $minlogs: The min of logs required
     * @return type
     */
    public static function getModulesFromCategories($categories, $minlogs) {
        global $DB;

        $ret = array();

        $modules = $DB->get_records_sql("SELECT id, name FROM {modules}");

        /**
         * Get values for each modules
         */
        foreach ($modules as $mod) {

            $ret[$mod->name] = 0;

            foreach ($categories as $category) {

                $number_of_courses = $DB->get_records_sql("SELECT count(*) as 'tot' FROM {course_modules} WHERE module = '" . $mod->id . "' AND course in (SELECT id FROM {course} WHERE category = ? AND id in (SELECT id_course FROM {report_moclog2_log} GROUP BY id_course HAVING SUM(tot_teacher_access) + SUM(tot_student_access)  > '" . $minlogs . "'))", array($category->id));

                $ret[$mod->name] += $number_of_courses[key($number_of_courses)]->tot;
            }
        }

        return $ret;
    }

    /**
     * Return an array with key: module, value: number of courses for the visibles modules
     * @global \report_moclog2\type $DB
     * @param type $categories: List of categories
     * @param type $minlogs: The min of logs required
     * @return type
     */
    public static function getModulesVisiblesFromCategories($categories, $minlogs) {
        global $DB;

        $ret = array();

        $modules = $DB->get_records_sql("SELECT id, name FROM {modules}");

        /**
         * Get values for each modules
         */
        foreach ($modules as $mod) {

            $ret[$mod->name] = 0;

            foreach ($categories as $category) {

                $number_of_courses = $DB->get_records_sql("SELECT count(*) as 'tot' FROM {course_modules} WHERE visible = 1 AND module = '" . $mod->id . "' AND course in (SELECT id FROM {course} WHERE category = ? AND id in (SELECT id_course FROM {report_moclog2_log} GROUP BY id_course HAVING SUM(tot_teacher_access) + SUM(tot_student_access)  > '" . $minlogs . "'))", array($category->id));

                $ret[$mod->name] += $number_of_courses[key($number_of_courses)]->tot;
            }
        }

        return $ret;
    }

    /**
     * Get the number of forums per course
     * @global \report_moclog2\type $DB
     * @param type $categories: List of categories
     * @param type $minlogs: The min of logs required
     * @return type
     */
    public static function getNumberOfForumsPerCourse($categories, $minlogs) {
        global $DB;

        $ret = array();
        foreach ($categories as $category) {
            $forums = $DB->get_records_sql("SELECT tot, count(*) 'number_of_courses' FROM (SELECT course, count(*) 'tot' FROM {forum} WHERE course in (SELECT id FROM {course} WHERE category = ? AND id in (SELECT id_course FROM {report_moclog2_log} GROUP BY id_course HAVING SUM(tot_teacher_access) + SUM(tot_student_access)  > '" . $minlogs . "')) GROUP BY course) temp GROUP BY tot", array($category->id));
            foreach ($forums as $forum) {

                if ($ret[$forum->number_of_courses] == null) {
                    $ret[$forum->number_of_courses] = $forum->tot;
                } else {
                    $ret[$forum->number_of_courses] += $forum->tot;
                }
            }
        }
        
        return $ret;
    }

    /**
     * Return the list of visibles modules
     * @global \report_moclog2\type $DB
     * @return type
     */
    public static function getModulesList() {
        global $DB;
        return $DB->get_records_sql("SELECT id, name FROM {modules} WHERE visible = 1 ORDER BY name ASC");
    }

    /**
     * Get the average of resources / activities from a list of courses and modules
     * @global \report_moclog2\type $DB
     * @param type $courses: List of courses
     * @param type $modules: List of modules
     * @param type $visible: true or false
     * @return type
     */
    public static function getAverageModules($courses, $modules, $visible) {
        global $DB;
        if (count($courses) > 0) {
            $totModules = $DB->get_records_sql("SELECT count(*) as 'tot' FROM {course_modules} WHERE course IN (" . implode(',', $courses) . ") AND module IN (SELECT id FROM {modules} WHERE name IN ('" . implode('\',\'', $modules) . "')) AND visible = " . intval($visible));
            return $totModules[key($totModules)]->tot / count($courses);
        } else
            return 0;
    }

    /**
     * Get the number of messages from a list of courses
     * @global \report_moclog2\type $DB
     * @param type $courses: List of courses
     * @param type $creation_start: Timestamp
     * @param type $creation_end: Timestamp
     * @return int: Number of messages in courses
     */
    public static function getNumberOfMessagesFromCourses($courses, $creation_start, $creation_end) {
        global $DB;
        if (count($courses) > 0) {
            $numOfMessages = $DB->get_records_sql("SELECT count(*) as numberofmessages FROM {forum_posts}, {forum_discussions} WHERE {forum_discussions}.id = {forum_posts}.discussion AND timemodified >= " . $creation_start . " AND timemodified <= " . $creation_end . " AND {forum_discussions}.course IN (" . implode(",", $courses) . ")");
            return $numOfMessages[key($numOfMessages)]->numberofmessages;
        } else
            return 0;
    }
    
    
    public static function getNumberOfChatMessagesFromCourses($courses, $creation_start, $creation_end){
        global $DB;
        if(count($courses) > 0){
            $numMessages = $DB->get_records_sql("SELECT count(*) as numberofmessages FROM {chat_messages}, {chat} WHERE {chat}.id = {chat_messages}.chatid AND timestamp >= " . $creation_start . " AND timestamp <= " . $creation_end . " AND course IN (" . implode(",", $courses) . ")");
            return $numMessages[key($numMessages)]->numberofmessages;
        } else return 0;
    }

    /**
     * Get the number of submissions from courses
     * @global \report_moclog2\type $DB
     * @param type $courses: List of courses
     * @param type $creation_start: Timestamp
     * @param type $creation_end: Timestamp
     * @return type
     */
    public static function getNumberOfSubmissionsFromCourses($courses, $creation_start, $creation_end) {
        global $DB;
        if (count($courses) > 0) {

            $query = "SELECT count(*) as tot FROM {assign_submission} WHERE timecreated <= " . $creation_end . " AND timecreated >= " . $creation_start . " AND assignment IN (SELECT id FROM {assign} WHERE course IN (" . implode(",", $courses) . ") ) ";

            $numberOfSubmissions = $DB->get_records_sql($query);

            return $numberOfSubmissions[key($numberOfSubmissions)]->tot;
        } else
            return 0;
    }

    /**
     * Get the number of attemps in quiz from a list of courses
     * @global \report_moclog2\type $DB
     * @param type $courses: List of courses
     * @param type $creation_start: Timestamp
     * @param type $creation_end: Timestamp
     * @return int
     */
    public static function getNumberOfQuizAttempsFromCourses($courses, $creation_start, $creation_end) {
        global $DB;
        if (count($courses) > 0) {

            $query = "SELECT count(*) as tot FROM {quiz_attempts} WHERE timefinish <= " . $creation_end . " AND timestart >= " . $creation_start . " AND state = 'finished' AND quiz IN (SELECT id FROM {quiz} WHERE course IN (" . implode(",", $courses) . ") ) ";

            $quizAttemps = $DB->get_records_sql($query);

            return $quizAttemps[key($quizAttemps)]->tot;
        } else
            return 0;
    }

}
