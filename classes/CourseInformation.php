<?php

/**
 * MOCLog2 report
 *
 * @package    report_moclog2
 * @copyright  eLab Yann Cuttaz, Christian Milani, Riccardo Mazza
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace report_moclog2;

class CourseInformation {

    public $id_course;
    public $category;
    public $visible;
    public $creation_date;
    public $teachers_enrolled = 0;
    public $students_enrolled = 0;

    public function __construct($id, $category, $visible, $creation_date) {
        $this->id_course = intval($id);
        $this->category = $category;
        $this->visible = boolval($visible);
        $this->creation_date = intval($creation_date);
    }

    /**
     * Return the users enrolled in courses from the role id
     * @global type $DB
     * @param type $roles_id
     * @return type: Array of id (stringed)
     */
    public static function getUsersEnrolled($roles_id) {
        global $DB;
        return $DB->get_records_sql("SELECT mcrs.id as 'courseid', count(*) 'tot_users_enrolled' FROM {role_assignments} as mra, {context} as mctxt, {course} as mcrs, {role} as mr WHERE mra.roleid = mr.id AND mr.id IN (" . $roles_id . ") AND mra.contextid = mctxt.id  AND mctxt.contextlevel = 50  AND mctxt.instanceid = mcrs.id GROUP BY mcrs.id");
    }
    
    /**
     * Return the first log timestamp creation of standard log table
     */
    public static function getFirstLogDate(){
        global $DB;
        $resultquery = $DB->get_records_sql("SELECT MIN(timecreated) 'min_log' FROM {logstore_standard_log}");
        return intval($resultquery[key($resultquery)]->min_log);
    }

}
