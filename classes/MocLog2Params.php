<?php

/**
 * MOCLog2 report
 *
 * @package    report_moclog2
 * @copyright  eLab Yann Cuttaz, Christian Milani, Riccardo Mazza
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace report_moclog2;

class MocLog2Params {

    /**
     * Filter params
     */
    public $hidden_courses;
    public $logs_required;
    public $min_enrolled_students;
    public $min_enrolled_teachers;
    public $course_creation;
    public $log_creation_start;
    public $log_creation_end;
    public $teachers = array();
    public $students = array();

    /**
     * Categories
     */
    public $categories;

    /**
     * Results
     */
    public $modules_to_show;
    public $general_report;
    public $modules_activities;

    /**
     * Classes
     * Associative array (key = id_module, value = array of classes)
     */
    public $classes = array();

    public function __construct() {
        
    }

    /**
     * Save params in session
     */
    public function Save() {
        $_SESSION['report_moclog2_params'] = json_encode($this);
    }

    /**
     * Add the ModuleClasses to classes params
     * @param type $moduleid
     * @param type $newclass: Array of ModuleClasses Object
     */
    public function addClasses($moduleid, $newclasses) {
        if ($this->classes == null)
            $this->classes = array();
        if (!isset($this->classes[$moduleid]) || $this->classes[$moduleid] == null)
            $this->classes[$moduleid] = array();
        foreach ($newclasses as $newclass) {
            $this->classes[$moduleid][] = $newclass;
        }
    }

    /**
     * Return an array of ModuleClass
     * @param type $moduleid
     * @return type
     */
    public function getClasses($moduleid) {
        if (!is_array($this->classes)) {
            $this->classes = get_object_vars($this->classes);
        }
        return $this->classes[$moduleid];
    }

    /**
     * Load params and destroy session
     */
    public static function Load($destroy_session = true) {
        if (isset($_SESSION['report_moclog2_params'])) {
            $obj = json_decode($_SESSION['report_moclog2_params']);
            if ($destroy_session) {
                MocLog2Params::destroy_session();
            }
            return MocLog2Params::cast($obj);
        } else {
            redirect("index.php");
        }
    }

    /**
     * Cast a json MocLog2Params to an object
     * @param type $obj
     * @return \report_moclog2\MocLog2Params
     */
    public static function cast($obj) {

        $ret = new MocLog2Params();

        $ret->categories = $obj->categories;
        $ret->course_creation = $obj->course_creation;
        $ret->hidden_courses = $obj->hidden_courses;
        $ret->logs_required = $obj->logs_required;
        $ret->min_enrolled_students = $obj->min_enrolled_students;
        $ret->min_enrolled_teachers = $obj->min_enrolled_teachers;
        $ret->modules_to_show = $obj->modules_to_show;
        $ret->general_report = $obj->general_report;
        $ret->log_creation_start = $obj->log_creation_start;
        $ret->log_creation_end = $obj->log_creation_end;
        $ret->classes = $obj->classes;
        $ret->modules_activities = $obj->modules_activities;

        return $ret;
    }

    /**
     * Destroy the moclog2 params session
     */
    public static function destroy_session() {
        $_SESSION['report_moclog2_params'] = null;
    }

    /**
     * Return true if the Moclog cron runned al least one time
     * @global type $DB
     * @return type
     */
    public static function cronRunned() {
        global $DB;
        $lastRunTimestampResultQuery = $DB->get_records_sql("SELECT value FROM {report_moclog2_settings} WHERE `key_name` = ?", array('last_run_end'));
        return intval($lastRunTimestampResultQuery[key($lastRunTimestampResultQuery)]->value) > 0;
    }

    /**
     * Return the last run time of cron
     * @global \report_moclog2\type $DB
     * @return type: Timestamp
     */
    public static function getCronLastRunTime() {
        global $DB;
        $lastRunTimestampResultQuery = $DB->get_records_sql("SELECT value FROM {report_moclog2_settings} WHERE `key_name` = ?", array('last_run_end'));
        return intval($lastRunTimestampResultQuery[key($lastRunTimestampResultQuery)]->value);
    }

    
    
}
