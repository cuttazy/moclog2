<?php

/**
 * MOCLog2 report
 *
 * @package    report_moclog2
 * @copyright  eLab Yann Cuttaz, Christian Milani, Riccardo Mazza
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace report_moclog2;

require_once $CFG->libdir . '/formslib.php';

class ClassesForm extends \moodleform {

    protected function definition() {
        global $CFG;

        $form = $this->_form;

        /**
         * Load modules to show
         */
        $params = \report_moclog2\MocLog2Params::Load(false);
        $modules = $params->modules_to_show;

        /**
         * For each module, create an hidden field 
         * with array value to set the module's classes
         */
        foreach ($modules as $moduleid) {
            
            $modulename = ModulesManagement::getModuleNameById($moduleid);
            $modulename = get_string('pluginname', 'mod_' . $modulename);
            
            $form->addElement('hidden', 'module_' . $moduleid, "[]");
            $form->setType('module_' . $moduleid, PARAM_RAW);
            
            /**
             * Fieldset
             */
            $form->addElement('header', 'fieldset_' . $moduleid, $modulename);
            
            /**
             * Classes
             */
            $form->addElement('html', '<div class="title_module">' . $modulename . '<span class="addClassButton" onclick="addClassModule(' . $moduleid . ')">+</span></div>');
            $form->addElement('html', '<div class="form_module" id="form_module_' . $moduleid . '">');
            
            for ($i = 0; $i < 5; $i++) {
                if ($i == 0)
                    $module_class = new ModuleClass($i, $i);
                else
                    $module_class = new ModuleClass($i * 5 - 4, $i * 5);

                $form->addElement('html', '<div class="module_class">');
                $form->addElement('html', '<div class="name_form">Class ' . ($i + 1) . '</div>');
                $form->addElement('html', '<div class="rule_name">must be ≥</div>');
                $form->addElement('html', '<div class="rule_input"><input type="number" class="rule_higher_of" value="' . $module_class->higher_of . '"></div>');
                $form->addElement('html', '<div class="rule_name">must be <=</div>');
                $form->addElement('html', '<div class="rule_input"><input type="number" class="rule_lowest_of" value="' . $module_class->lowest_of . '"></div>');
                $form->addElement('html', '<div class="deleteButton" onclick="deleteLastChild(' . $moduleid . ')">delete</div>');
                $form->addElement('html', '</div>');
            }
            $form->addElement('html', '</div>');
            
        }

        /**
         * Submit Button
         */
        $buttonarray = array();
        $buttonarray[] = $this->_form->createElement('submit', 'previous_button_classes', "Previous");
        $buttonarray[] = $this->_form->createElement('submit', 'search_button_classes', "Search");
        $this->_form->addGroup($buttonarray, 'buttonar', '', array(' '), false);
        $this->_form->closeHeaderBefore('buttonar');
    }

    function validation($data, $files) {
        return array();
    }

}
