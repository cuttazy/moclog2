<?php

/**
 * MOCLog2 report
 *
 * @package    report_moclog2
 * @copyright  eLab Yann Cuttaz, Christian Milani, Riccardo Mazza
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace report_moclog2\task;

defined('MOODLE_INTERNAL') || die();

class moclog2_cron_collectdata extends \core\task\scheduled_task {

    private $studentsSelected = null;
    private $teachersSelected = null;
    
    public function get_name() {
        return get_string('cron_description', 'report_moclog2');
    }

    public function execute() {

        global $DB;

        ini_set('max_execution_time', 36000);
        ini_set('memory_limit', '2048M');

        $initial_memory_usage = memory_get_usage();

        /**
         *  trace start
         */
        mtrace("\nMoclog2 - task (start)!");
        
        
        /**
         * Check teachers & students selected in settings
         */
        mtrace("\nMoclog2 - Check moclog2 settings");
        $this->studentsSelected = \report_moclog2\CronConfig::GetStudentsSelected();
        $this->teachersSelected = \report_moclog2\CronConfig::GetTeachersSelected();
        if($this->studentsSelected == null || $this->teachersSelected == null){
            mtrace("\nMoclog2 - ERROR: please select at least 1 student and 1 teacher in roles settings of plugin.");
        }
        else{

            /**
             * Get last run timestamp
             */
            $lastRunTimestampResultQuery = $DB->get_records_sql("SELECT value FROM {report_moclog2_settings} WHERE `key_name` = ?", array('last_run_end'));
            if (count($lastRunTimestampResultQuery) > 0) {
                $lastRunTimestamp = intval($lastRunTimestampResultQuery[key($lastRunTimestampResultQuery)]->value);
            } else {
                $lastRunTimestamp = 0;
                mtrace("\nMoclog2 - Insert default timestamp in settings table.");
                $DB->execute("INSERT INTO {report_moclog2_settings} VALUES (1, 'last_run_start', 0)");
                $DB->execute("INSERT INTO {report_moclog2_settings} VALUES (2, 'last_run_end', 0)");
            }

            if ($lastRunTimestamp == 0) {
                $DB->delete_records('report_moclog2_courses');
            }

            mtrace("\nMoclog2 - Update start timestamp.");
            $DB->execute("UPDATE {report_moclog2_settings} SET value = ? WHERE `key_name` = 'last_run_start'", array(time()));

            /**
             * Get all courses
             */
            $coursesID = $DB->get_records_sql("SELECT mc.id, mcc.path as category, mc.visible, mc.timecreated FROM {course} as mc, {course_categories} as mcc WHERE mcc.id = mc.category");

            /**
             * Save courses informations
             */
            $this->saveCoursesInformations($coursesID);

            /**
             * Check if is the first run
             */
            if ($lastRunTimestamp == 0) {

                /**
                 * Get first log created time
                 */
                $firstLogResultQuery = $DB->get_records_sql("SELECT timecreated FROM {logstore_standard_log} ORDER BY `timecreated` ASC LIMIT 1");
                $firstLogTimeStamp = $firstLogResultQuery[key($firstLogResultQuery)]->timecreated;
                $lastTimeStampToCheck = time();
                $timeMax = time() + 1;

                /**
                 * Create logs for each day
                 */
                while ($firstLogTimeStamp < $lastTimeStampToCheck) {
                    $this->addAndSaveCoursesLogs($coursesID, $lastTimeStampToCheck, $timeMax);
                    $timeMax = $lastTimeStampToCheck;
                    $lastTimeStampToCheck = strtotime("-1 day", $lastTimeStampToCheck);
                }
            } else {
                $this->addAndSaveCoursesLogs($coursesID, $lastRunTimestamp);
            }

            mtrace("\n\nMoclog2 - Memory usage: " . ((memory_get_usage() - $initial_memory_usage) / 1024 / 1024) . "M \n");

            /**
             * Update timestamp
             */
            mtrace("\nMoclog2 - Update end timestamp.");
            $DB->execute("UPDATE {report_moclog2_settings} SET value = ? WHERE `key_name` = 'last_run_end'", array(time()));
        }
        
        /**
         *  trace end
         */
        mtrace("\nMoclog2 - task (end)!");
    }

    /**
     * Add courses log to array
     * @global type $DB
     * @param type $courseslog
     * @param type $coursesID
     * @param type $timeToCheck
     */
    private function addAndSaveCoursesLogs($coursesID, $timeToCheck, $timeMax = null) {

        global $DB;

        $courseslog = array();

        if ($timeMax == null) {
            $timeMax = time() + 1;
        }

        /**
         * Get students and teachers activities on courses
         */
        $courseslogsstudents = $DB->get_records_sql("SELECT courseid, count(*) 'tot_student_access' FROM {logstore_standard_log} WHERE timecreated > ? AND timecreated <= ? AND userid IN (SELECT userid FROM {role_assignments} WHERE roleid IN (" . $this->studentsSelected . ")) GROUP BY courseid", array($timeToCheck, $timeMax));
        $courseslogsteachers = $DB->get_records_sql("SELECT courseid, count(*) 'tot_teacher_access' FROM {logstore_standard_log} WHERE timecreated > ? AND timecreated <= ? AND userid IN (SELECT userid FROM {role_assignments} WHERE roleid IN (" . $this->teachersSelected . ")) GROUP BY courseid", array($timeToCheck, $timeMax));
        foreach ($coursesID as $course) {

            $courseid = $course->id;

            /**
             * Create CourseLog object
             */
            $courselog = new \report_moclog2\CourseLog($course->id);
            $courselog->export_time = $timeToCheck;

            /**
             * Insert into object activities params
             */
            foreach ($courseslogsstudents as $courselogStudent) {
                if ($courselogStudent->courseid == $courseid) {
                    $courselog->tot_student_access = $courselogStudent->tot_student_access;
                    break;
                }
            }
            foreach ($courseslogsteachers as $courselogTeacher) {
                if ($courselogTeacher->courseid == $courseid) {
                    $courselog->tot_teacher_access = $courselogTeacher->tot_teacher_access;
                    break;
                }
            }

            /**
             * Add object to array
             */
            $courseslog[] = $courselog;
        }

        /**
         * Save array in DB
         */
        mtrace("\nMoclog2 - Insert Courses log: Timestamp of courses: " . $timeToCheck);
        $DB->insert_records('report_moclog2_log', $courseslog);

        unset($courseslog);
        unset($courseslogsstudents);
        unset($courseslogsteachers);
    }

    /**
     * Save courses informations
     * @param type $coursesID
     */
    private function saveCoursesInformations($coursesID) {
        global $DB;

        $courses_informations = array();

        /**
         * Get students and teachers enrollments
         */
        $teachersEnrolled = \report_moclog2\CourseInformation::getUsersEnrolled($this->teachersSelected);
        $studentsEnrolled = \report_moclog2\CourseInformation::getUsersEnrolled($this->studentsSelected);

        foreach ($coursesID as $course) {

            /**
             * Course informations
             */
            $courseinfo = new \report_moclog2\CourseInformation($course->id, $course->category, $course->visible, $course->timecreated);
            if (isset($studentsEnrolled[$course->id]) && $studentsEnrolled[$course->id] != null) {
                $courseinfo->students_enrolled = intval($studentsEnrolled[$course->id]->tot_users_enrolled);
            }
            if (isset($teachersEnrolled[$course->id]) && $teachersEnrolled[$course->id] != null) {
                $courseinfo->teachers_enrolled = intval($teachersEnrolled[$course->id]->tot_users_enrolled);
            }
            $courses_informations[] = $courseinfo;
        }

        mtrace("\nMoclog2 - Update courses informations");
        $DB->delete_records('report_moclog2_courses');
        $DB->insert_records('report_moclog2_courses', $courses_informations);

        unset($courses_informations);
        unset($coursesID);
    }
    
}
