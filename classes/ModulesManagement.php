<?php

/**
 * MOCLog2 report
 *
 * @package    report_moclog2
 * @copyright  eLab Yann Cuttaz, Christian Milani, Riccardo Mazza
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace report_moclog2;

class ModulesManagement {

    /**
     * Get the module name by ID
     * @global \report_moclog2\type $DB
     * @param type $moduleid: ID of module searched
     * @return type
     */
    public static function getModuleNameById($moduleid) {
        global $DB;
        $name = $DB->get_records_sql("SELECT name FROM {modules} WHERE id = '$moduleid'");
        return $name[key($name)]->name;
    }
    
    public static function getModuleIdByName($modulename){
        global $DB;
        $module = $DB->get_records_sql("SELECT id FROM {modules} WHERE name = '$modulename'");
        return $module[key($module)]->id;
    }

    /**
     * Get an array with modules separated in activities and resources
     * @return type
     */
    public static function getModulesTypes() {

        $modnames = get_module_types_names();

        // Retrieve all modules with associated metadata
        $modules = get_module_metadata(get_course(1), $modnames);

        // We'll sort resources and activities into two lists
        $activities = array(MOD_CLASS_ACTIVITY => array(), MOD_CLASS_RESOURCE => array());

        foreach ($modules as $module) {

            if (isset($module->types)) {
                // Sort module subtypes into the list
                $activityclass = MOD_CLASS_ACTIVITY;
                if ($module->archetype == MOD_CLASS_RESOURCE) {
                    $activityclass = MOD_CLASS_RESOURCE;
                }
                if (!empty($module->name)) {
                    // This grouping has a name
                    $activities[$activityclass][] = array($module->name => $subtypes);
                } else {
                    // This grouping does not have a name
                    $activities[$activityclass] = array_merge($activities[$activityclass], $subtypes);
                }
            } else {
                // This module has no subtypes
                $activityclass = MOD_CLASS_ACTIVITY;
                if ($module->archetype == MOD_ARCHETYPE_RESOURCE) {
                    $activityclass = MOD_CLASS_RESOURCE;
                } else if ($module->archetype === MOD_ARCHETYPE_SYSTEM) {
                    // System modules cannot be added by user, do not add to dropdown
                    continue;
                }
                $activities[$activityclass][] = $module->name;
            }
        }

        return $activities;
    }

    /**
     * Return the number of modules from courses
     * @global \report_moclog2\type $DB
     * @param type $courses: List of courses
     * @param type $moduleid: int
     * @return type: Number of modules in courses
     */
    public static function getNumberOfModulesPerCourses($courses, $moduleid) {
        global $DB;
        if (count($courses) > 0) {
            $tot = $DB->get_records_sql("SELECT count(*) as 'tot' FROM {course_modules} WHERE module = '$moduleid' AND course IN (" . implode(",", $courses) . ")");
            return $tot[key($tot)]->tot;
        } else
            return 0;
    }

}
