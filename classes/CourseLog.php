<?php

/**
 * MOCLog2 report
 *
 * @package    report_moclog2
 * @copyright  eLab Yann Cuttaz, Christian Milani, Riccardo Mazza
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace report_moclog2;

class CourseLog {

    public $id;
    public $id_course;
    public $tot_student_access = 0;
    public $tot_teacher_access = 0;
    public $export_time;

    public function __construct($courseid) {
        $this->id_course = $courseid;
    }

    /**
     * Get the average of access for a teacher or a student in courses
     * @global \report_moclog2\type $DB
     * @param type $courses: Array of courses id.
     * @param type $userrole: teacher or student.
     * @param type $log_creation_start: Log from
     * @param type $log_creation_end: Log end
     * @return int: Number of access
     */
    public static function getAverageAccess($courses, $userrole, $log_creation_start, $log_creation_end) {
        global $DB;
        if (count($courses) > 0) {
            $query_log_creation_start = ($log_creation_start == 0) ? "" : " AND export_time >= " . $log_creation_start;
            $query_log_creation_end = ($log_creation_end == 0) ? "" : " AND export_time <= " . $log_creation_end;
            $result = $DB->get_records_sql("SELECT id_course, AVG(tot_" . $userrole . "_access) as aver, SUM(tot_" . $userrole . "_access) as sum FROM {report_moclog2_log} WHERE id_course IN (" . implode(',', $courses) . ") " . $query_log_creation_start . $query_log_creation_end);
            return $result[key($result)]->aver;
        } else
            return 0;
    }

    /**
     * Get the first log date from moclog2_log table
     * @global type $DB
     * @return int
     */
    public static function getFirstLogDate() {
        global $DB;
        $result = $DB->get_records_sql("SELECT MIN(`export_time`) as min_export_time FROM {report_moclog2_log}");
        if (count($result) > 0) {
            return $result[key($result)]->min_export_time;
        } else
            return 0;
    }

    /**
     * Get the average of access for a student in courses
     * @param type $courses: Array of courses id
     * @param type $log_creation_start: Log from
     * @param type $log_creation_end: Log end
     * @return type
     */
    public static function getAverageStudentsAccess($courses, $log_creation_start, $log_creation_end) {
        return CourseLog::getAverageAccess($courses, "student", $log_creation_start, $log_creation_end);
    }

    /**
     * * Get the average of access for a teacher in courses
     * @param type $courses: Array of courses id
     * @param type $log_creation_start: Log from
     * @param type $log_creation_end: Log end
     * @return type
     */
    public static function getAverageTeacherAccess($courses, $log_creation_start, $log_creation_end) {
        return CourseLog::getAverageAccess($courses, "teacher", $log_creation_start, $log_creation_end);
    }

}
