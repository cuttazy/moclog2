<?php

/**
 * MOCLog2 report
 *
 * @package    report_moclog2
 * @copyright  eLab Yann Cuttaz, Christian Milani, Riccardo Mazza
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace report_moclog2;

class ModuleClass {

    public $higher_of;
    public $lowest_of;

    public function __construct($higher_of, $lowest_of) {
        if ($higher_of == null)
            $higher_of = 0;

        $this->higher_of = $higher_of;
        $this->lowest_of = $lowest_of;
    }

}
