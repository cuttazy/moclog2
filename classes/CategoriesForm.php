<?php

/**
 * MOCLog2 report
 *
 * @package    report_moclog2
 * @copyright  eLab Yann Cuttaz, Christian Milani, Riccardo Mazza
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace report_moclog2;

require_once $CFG->libdir . '/formslib.php';

class CategoriesForm extends \moodleform {

    protected function definition() {
        global $CFG;

        $form = $this->_form;

        /**
         * Categories to show
         */
        $form->addElement('hidden', 'categories_to_show', "[]");
        $form->setType('categories_to_show', PARAM_RAW);

        /**
         * Submit Button
         */
        $this->add_action_buttons(false, "Next");
    }

    function validation($data, $files) {
        return array();
    }
    
}
