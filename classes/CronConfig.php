<?php

/**
 * MOCLog2 report
 *
 * @package    report_moclog2
 * @copyright  eLab Yann Cuttaz, Christian Milani, Riccardo Mazza
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace report_moclog2;

class CronConfig {

    
    /**
     * Return all roles of {role} table
     * @global type $DB
     * @return type
     */
    public static function GetAllRoles(){
        global $DB;
        return $DB->get_records_sql("SELECT id, shortname FROM {role}");
    }
    
    /**
     * Return the list of students selected
     * @global \report_moclog2\type $DB
     * @return type
     */
    public static function GetStudentsSelected(){
        global $DB;
        $selected = $DB->get_records_sql("SELECT value FROM {config_plugins} WHERE plugin = 'report_moclog2' AND name = 'students_role'");
        return $selected[key($selected)]->value;
    }
    
    /**
     * Return the list of teachers selected
     * @global \report_moclog2\type $DB
     * @return type
     */
    public static function GetTeachersSelected(){
        global $DB;
        $selected = $DB->get_records_sql("SELECT value FROM {config_plugins} WHERE plugin = 'report_moclog2' AND name = 'teachers_role'");
        return $selected[key($selected)]->value;
    }

}
