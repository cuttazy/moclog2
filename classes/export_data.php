<?php

/**
 * MOCLog2 report
 *
 * @package    report_moclog2
 * @copyright  eLab Yann Cuttaz, Christian Milani, Riccardo Mazza
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace report_moclog2;

class export_data {

    private static $filename = "moclog2_report.xlsx";
    private static $secondfilename = "moclog2_modules";

    /**
     * Create the general reports, containing overall statistics for the selected categories
     * @global type $CFG
     * @param type $params
     */
    public static function CreateReportExcel($params) {


        global $CFG;
        require_once($CFG->dirroot . '/lib/excellib.class.php');

        /**
         * Create file
         */
        $file1 = new \MoodleExcelWorkbook("-");
        $file1->send(export_data::$filename);

        /**
         * Excel format
         */
        $bold = new \MoodleExcelFormat();
        $bold->set_bold();

        $titleDocument = new \MoodleExcelFormat();
        $titleDocument->set_bold();
        $titleDocument->set_size(18);

        $titleColumn = new \MoodleExcelFormat();
        $titleColumn->set_text_wrap();

        /**
         * Get modules types
         */
        $modules = $params->modules_activities;


        /**
         * Get courses informations
         */
        $course_categories = export_data::getCoursesCategories($params);

        $worksheet = $file1->add_worksheet("General reports");
        $worksheet->write_string(1, 1, "General reports", $titleDocument);
        
        /**
         * Last run cron
         */
        $lastRunTimestamp = \report_moclog2\MocLog2Params::getCronLastRunTime();
        $worksheet->write_string(1, 4, "Cron last run: " . date('d.m.Y', $lastRunTimestamp));
        


        $row = 4;
        $printTitles = true;

        foreach ($course_categories as $categoryid => $courses) {

            $category = \report_moclog2\CategoriesManagement::getCategoryByID($categoryid);


            // Start index of columns in Excel file
            $col = 1;

            $worksheet->write_string($row, $col, $category->name);
            $col++;
            // Get number of courses from category
            $totCourses = count($courses);
            export_data::writeNumberInWorksheet($worksheet, $row, $col, $totCourses, $printTitles, get_string('report_numberofcourses', 'report_moclog2'), $titleColumn);
            
            // Get number of empty courses from category
            $totEmptyCourses = \report_moclog2\CoursesManagement::getTotEmptyCourses($courses);
            export_data::writeNumberInWorksheet($worksheet, $row, $col, $totEmptyCourses, $printTitles, get_string('report_numberofemptycourses', 'report_moclog2'), $titleColumn);
            
            // Average of students per course
            $totStudentsEnrolled = \report_moclog2\CoursesManagement::getTotStudentsEnrolledFromCourses($courses);
            $avgPerCourse = ($totCourses == 0) ? 0 : $totStudentsEnrolled / $totCourses;
            export_data::writeNumberInWorksheet($worksheet, $row, $col, $avgPerCourse, $printTitles, get_string('report_averageofstudentspercourse', 'report_moclog2'), $titleColumn);

            // Average of teachers per course
            $totTeachersEnrolled = \report_moclog2\CoursesManagement::getTotTeachersEnrolledFromCourses($courses);
            $avgPerCourse = ($totCourses == 0) ? 0 : $totTeachersEnrolled / $totCourses;
            export_data::writeNumberInWorksheet($worksheet, $row, $col, $avgPerCourse, $printTitles, get_string('report_averageofteachersspercourse', 'report_moclog2'), $titleColumn);

            // Average of access per student / day
            $averageStudentAccess = \report_moclog2\CourseLog::getAverageStudentsAccess($courses, $params->log_creation_start, $params->log_creation_end);
            export_data::writeNumberInWorksheet($worksheet, $row, $col, $averageStudentAccess, $printTitles, get_string('report_averageofstudentslogperday', 'report_moclog2'), $titleColumn);

            // Average of access per teacher / day
            $averageTeacherAccess = \report_moclog2\CourseLog::getAverageTeacherAccess($courses, $params->log_creation_start, $params->log_creation_end);
            export_data::writeNumberInWorksheet($worksheet, $row, $col, $averageTeacherAccess, $printTitles, get_string('report_averageofteacherslogperday', 'report_moclog2'), $titleColumn);
            
            // Average of visible resources per course
            $averageResources = \report_moclog2\CoursesManagement::getAverageModules($courses, $modules[1], true);
            export_data::writeNumberInWorksheet($worksheet, $row, $col, $averageResources, $printTitles, get_string('report_averagevisibleresourcespercourse', 'report_moclog2'), $titleColumn);

            // Average of hidden resources per course
            $averageResources = \report_moclog2\CoursesManagement::getAverageModules($courses, $modules[1], false);
            export_data::writeNumberInWorksheet($worksheet, $row, $col, $averageResources, $printTitles, get_string('report_averagehiddenresourcespercourse', 'report_moclog2'), $titleColumn);

            // Average of visible activities per course
            $averageActivities = \report_moclog2\CoursesManagement::getAverageModules($courses, $modules[0], true);
            export_data::writeNumberInWorksheet($worksheet, $row, $col, $averageActivities, $printTitles, get_string('report_averagevisibleactivitiespercourse', 'report_moclog2'), $titleColumn);

            // Average of hidden activities per course
            $averageActivities = \report_moclog2\CoursesManagement::getAverageModules($courses, $modules[0], false);
            export_data::writeNumberInWorksheet($worksheet, $row, $col, $averageActivities, $printTitles, get_string('report_averagehiddenactivitiespercourse', 'report_moclog2'), $titleColumn);

            // Average per module per course
            foreach ($params->modules_to_show as $moduleid) {

                $modulename = \report_moclog2\ModulesManagement::getModuleNameById($moduleid);

                $numModules = \report_moclog2\ModulesManagement::getNumberOfModulesPerCourses($courses, $moduleid);
                $avgPerCourse = ($totCourses == 0) ? 0 : $numModules / $totCourses;
                export_data::writeNumberInWorksheet($worksheet, $row, $col, $avgPerCourse, $printTitles, get_string('report_averageof', 'report_moclog2') . get_string('pluginname', 'mod_' . $modulename) . get_string('report_percourse', 'report_moclog2'), $titleColumn);
            }

            // Average of messages per forum
            $numMessages = \report_moclog2\CoursesManagement::getNumberOfMessagesFromCourses($courses, $params->log_creation_start, $params->log_creation_end);
            $avgPerForum = ($totCourses == 0) ? 0 : $numMessages / $totCourses;
            export_data::writeNumberInWorksheet($worksheet, $row, $col, $avgPerForum, $printTitles, get_string('report_averagemessagesforumpercourse', 'report_moclog2'), $titleColumn);
            
            // Number of submissions (per category)
            $numSubmissions = \report_moclog2\CoursesManagement::getNumberOfSubmissionsFromCourses($courses, $params->log_creation_start, $params->log_creation_end);

            // Number of submissions per course
            $avgPerCourse = ($totCourses == 0) ? 0 : $numSubmissions / $totCourses;
            export_data::writeNumberInWorksheet($worksheet, $row, $col, $avgPerCourse, $printTitles, get_string('report_numberofassignmentpercourse', 'report_moclog2'), $titleColumn);

            // Number of submissions per student
            $avgPerStudent = ($totCourses == 0 || $totStudentsEnrolled == 0) ? 0 : $numSubmissions / $totStudentsEnrolled ;
            export_data::writeNumberInWorksheet($worksheet, $row, $col, $avgPerStudent, $printTitles, get_string('report_numberofassignmentperstudent', 'report_moclog2'), $titleColumn);
            
            // Number of chat messages
            $numMessages = CoursesManagement::getNumberOfChatMessagesFromCourses($courses, $params->log_creation_start, $params->log_creation_end);
            
            // Number of chat messages per course
            $avgPerCourse = ($totCourses == 0) ? 0 : $numMessages / $totCourses;
            export_data::writeNumberInWorksheet($worksheet, $row, $col, $avgPerCourse, $printTitles, get_string('report_numberofchatmessagespercourse', 'report_moclog2'), $titleColumn);
            
            // Number of chat messages per student
            $avgPerStudent = ($totCourses == 0 || $totStudentsEnrolled == 0) ? 0 : $numMessages / ($totStudentsEnrolled / $totCourses);
            export_data::writeNumberInWorksheet($worksheet, $row, $col, $avgPerStudent, $printTitles, get_string('report_numberofchatmessagesperstudent', 'report_moclog2'), $titleColumn);
            
            // Number of quiz attemps
            $totAttemps = \report_moclog2\CoursesManagement::getNumberOfQuizAttempsFromCourses($courses, $params->log_creation_start, $params->log_creation_end);
            // Number of attemps per course
            $avgPerCourse = ($totCourses == 0) ? 0 : $totAttemps / $totCourses;
            export_data::writeNumberInWorksheet($worksheet, $row, $col, $avgPerCourse, $printTitles, get_string('report_numberofquizattempspercourse', 'report_moclog2'), $titleColumn);

            // Number of attemps per student
            $avgPerStudent = ($totCourses == 0 || $totStudentsEnrolled == 0) ? 0 : $totAttemps / ($totStudentsEnrolled / $totCourses);
            export_data::writeNumberInWorksheet($worksheet, $row, $col, $avgPerStudent, $printTitles, get_string('report_numberofquizattempsperstudent', 'report_moclog2'), $titleColumn);
            
            $printTitles = false;

            $row++;
        }





        $file1->close();
    }
    
    /**
     * Write a number in a MocLog Excel Worksheet
     * @param type $worksheet: Worksheet to edit
     * @param type $row: Row of number
     * @param type $col: Column of number
     * @param type $number: Number to write in worksheet
     * @param type $printTitles: Boolean to show if the title is to write (optional)
     * @param type $title: The title of column (optional, mandatory if $printTitles is true)
     * @param type $styleTitle: The style of title row (optional, mandatory if $printTitles is true)
     */
    private static function writeNumberInWorksheet(&$worksheet, $row, &$col, $number, $printTitles = false, $title = null, $styleTitle = null){
        if($printTitles)
            $worksheet->write_string($row - 1, $col, $title, $styleTitle);
        $worksheet->write_number($row, $col, $number);
        $col++;
    }

    /**
     * Get the courses searched with params, from a list of categories
     * @param type $params: The user params selected
     * @param type $getmodules: Add modules to returned array
     * @param type $moduleid: Set the module searched if $getmodules is true
     * @return type
     */
    private static function getCoursesCategories($params, $getmodules = false, $moduleid = null) {

        $course_categories = array();
        foreach ($params->categories as $categoryid) {
            $course_categories[$categoryid] = array();
            $courses = \report_moclog2\CoursesManagement::getCoursesFromCategoryId($categoryid, $params);
            
            if (count($courses) > 0 && $getmodules)
                $course_categories[$categoryid] = \report_moclog2\CoursesManagement::getModulesFromCoursesList($courses, $moduleid);
            else
                $course_categories[$categoryid] = array();

            if (!$getmodules) {


                $courses_id = array();
                foreach ($courses as $course) {
                    $courses_id[] = $course->id_course;
                }

                $course_categories[$categoryid] = $courses_id;
            }
        }

        return $course_categories;
    }

    /**
     * Create specific report (per modules)
     * @global type $CFG
     * @param type $params: User searched params
     * @param type $moduleid: the id of module for the report
     */
    public static function CreateModulesReportExcel($params, $moduleid) {
        global $CFG;
        require_once($CFG->dirroot . '/lib/excellib.class.php');

        $modulename = \report_moclog2\ModulesManagement::getModuleNameById($moduleid);

        /**
         * Create file
         */
        $file2 = new \MoodleExcelWorkbook("-");
        $file2->send(export_data::$secondfilename . "_" . $modulename);

        /**
         * Excel format
         */
        $bold = new \MoodleExcelFormat();
        $bold->set_bold();

        $titleDocument = new \MoodleExcelFormat();
        $titleDocument->set_bold();
        $titleDocument->set_size(18);

        $titleColumn = new \MoodleExcelFormat();
        $titleColumn->set_text_wrap();

        $classTitle = new \MoodleExcelFormat();
        $classTitle->set_text_wrap();
        $classTitle->set_border(1);



        /**
         * Get courses informations
         */
        $course_categories = export_data::getCoursesCategories($params, true, $moduleid);


        /**
         * Create worksheet
         */
        $worksheet = $file2->add_worksheet(get_string('pluginname', 'mod_' . $modulename));
        $worksheet->write_string(1, 1, get_string('pluginname', 'mod_' . $modulename), $titleDocument);

        /**
         * Last run cron
         */
        $lastRunTimestamp = \report_moclog2\MocLog2Params::getCronLastRunTime();
        $worksheet->write_string(1, 4, get_string("report_cronlastrun", "report_moclog2") . date('d.m.Y', $lastRunTimestamp));
        

        /**
         * Get Classes
         */
        $module_classes = $params->getClasses($moduleid);
        $colIndex = 2;
        $indexToCheck = count($module_classes) + $colIndex - 1;
        foreach ($module_classes as $mod_class) {
            $rowIndex = 3;
            $worksheet->write_string($rowIndex, $colIndex, $mod_class->higher_of . " - " . ($mod_class->lowest_of), $classTitle);
            
            // Check if the index of column is last column
            if ($indexToCheck == $colIndex) {
                $worksheet->write_string($rowIndex, $colIndex, " > " . $mod_class->higher_of, $classTitle);
            }
            /**
             * Write Categories values
             */
            foreach ($course_categories as $categoryid => $courses_modules) {



                $rowIndex++;


                $tot_courses = 0;



                foreach ($courses_modules as $course_module) {

                    // Check if the index of column is last column
                    if ($indexToCheck == $colIndex && $course_module->module == $moduleid && $course_module->count_module >= $mod_class->higher_of) {
                        $tot_courses++;
                    } else if ($course_module->module == $moduleid && $course_module->count_module >= $mod_class->higher_of && $course_module->count_module <= $mod_class->lowest_of) {
                        $tot_courses++;
                    }
                }


                $worksheet->write_number($rowIndex, $colIndex, $tot_courses);
            }

            $colIndex++;
        }
        
        /**
         * Get Categories informations
         */
        $rowIndex = 4;
        foreach ($params->categories as $categoryid) {
            $category = \report_moclog2\CategoriesManagement::getCategoryByID($categoryid);
            $worksheet->write_string($rowIndex, 1, $category->name, $classTitle);
            $rowIndex++;
        }




        $file2->close();
    }

}
