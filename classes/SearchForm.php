<?php

/**
 * MOCLog2 report
 *
 * @package    report_moclog2
 * @copyright  eLab Yann Cuttaz, Christian Milani, Riccardo Mazza
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace report_moclog2;

require_once $CFG->libdir . '/formslib.php';

class SearchForm extends \moodleform {

    private static $datefieldoptions = array('optional' => true, 'step' => 1);

    protected function definition() {
        global $CFG;

        $form = $this->_form;

        /**
         * Hidden Courses
         */
        $form->addElement('checkbox', 'show_hidden_courses', get_string('hiddencourses', 'report_moclog2'));
        $form->setType('show_hidden_courses', PARAM_BOOL);
        $form->setDefault('show_hidden_courses', false);
        $form->addHelpButton('show_hidden_courses', 'hiddencourses', 'report_moclog2');


        $form->addElement("static", "static_element", get_string('description_title', 'report_moclog2'), get_string('description_text', 'report_moclog2'));

        /**
         * Logs required
         */
        $form->addElement('text', 'logs_required', get_string('logsrequired', 'report_moclog2'));
        $form->setType('logs_required', PARAM_NUMBER);
        $form->setDefault('logs_required', "100");

        /**
         * Min Enrolled Students
         */
        $form->addElement('text', 'min_enrolled_students', get_string('minenrolledstudents', 'report_moclog2'));
        $form->setType('min_enrolled_students', PARAM_NUMBER);
        $form->setDefault('min_enrolled_students', "0");

        /**
         * Min Enrolled Teachers
         */
        $form->addElement('text', 'min_enrolled_teachers', get_string('minenrolledteachers', 'report_moclog2'));
        $form->setType('min_enrolled_teachers', PARAM_NUMBER);
        $form->setDefault('min_enrolled_teachers', "0");

        /**
         * Course Creation
         */
        $form->addElement('date_time_selector', 'course_creation', get_string('coursecreation', 'report_moclog2'), self::$datefieldoptions);
        $form->addHelpButton('course_creation', 'coursecreation', 'report_moclog2');


        /**
         * Log interval
         */
        $form->addElement('date_time_selector', 'log_creation_start', get_string('logcreationstart', 'report_moclog2'), self::$datefieldoptions);
        $form->setDefault('log_creation_start', CourseInformation::getFirstLogDate());
        $form->addHelpButton('log_creation_start', 'logcreationstart', 'report_moclog2');


        $form->addElement('date_time_selector', 'log_creation_end', get_string('logcreationend', 'report_moclog2'), self::$datefieldoptions);
        $form->setDefault('log_creation_end', mktime(0, 0, 0, date("m"), date("d"), date("Y")));
        $form->addHelpButton('log_creation_end', 'logcreationend', 'report_moclog2');

        /**
         * Submit Button
         */
        $buttonarray = array();
        $buttonarray[] = $this->_form->createElement('submit', 'previous_button_params', "Previous");
        $buttonarray[] = $this->_form->createElement('submit', 'next_button_params', "Next");
        $this->_form->addGroup($buttonarray, 'buttonar', '', array(' '), false);
        $this->_form->closeHeaderBefore('buttonar');
    }

    function validation($data, $files) {
        return array();
    }

}
