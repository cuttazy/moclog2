<?php

/**
 * MOCLog2 report
 *
 * @package    report_moclog2
 * @copyright  eLab Yann Cuttaz, Christian Milani, Riccardo Mazza
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace report_moclog2;

class Progression {
    
    /**
     * Return the progression at the current step
     * @param type $stepindex Page index
     */
    public static function getCurrentProgression($stepindex){
        return Progression::createProgression($stepindex, 5, array(
            get_string('progression_step1', 'report_moclog2'), 
            get_string('progression_step2', 'report_moclog2'), 
            get_string('progression_step3', 'report_moclog2'), 
            get_string('progression_step4', 'report_moclog2'), 
            get_string('progression_step5', 'report_moclog2')));
    }
    
    /**
     * Create page progression
     * @param type $stepindex
     * @param type $totalsteps
     * @param type $labels
     */
    public static function createProgression($stepindex, $totalsteps, $labels){
        global $DB;
        
        /**
         * Get settings values
         */
        $resultquery = $DB->get_records_sql("SELECT value FROM {config_plugins} WHERE plugin = 'report_moclog2' AND name = 'progression_border_color'");
        $bordercolor = $resultquery[key($resultquery)]->value;
        
        $resultquery = $DB->get_records_sql("SELECT value FROM {config_plugins} WHERE plugin = 'report_moclog2' AND name = 'progression_background_color'");
        $backgroundcolor = $resultquery[key($resultquery)]->value;
        
        $resultquery = $DB->get_records_sql("SELECT value FROM {config_plugins} WHERE plugin = 'report_moclog2' AND name = 'progression_background_color_active'");
        $backgroundcoloractive = $resultquery[key($resultquery)]->value;
        
        unset($resultquery);
        
        /**
         * Create steps progession
         */
        $ret = '<div class="pagination_advancement">';
        for($i = 0 ; $i < $totalsteps ; $i++){
            
            $background = ($stepindex == $i) ? $backgroundcoloractive : $backgroundcolor;
            $ret .= '<div class="pagination_box" style="background-color: '.$background.'; border-color: '.$bordercolor.' !important">'.  get_string('progression_step' . ($i+1), 'report_moclog2').'</div>';
            
        }
        $ret .= '</div>';
        
        return $ret;
    }
    
}
