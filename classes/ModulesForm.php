<?php

/**
 * MOCLog2 report
 *
 * @package    report_moclog2
 * @copyright  eLab Yann Cuttaz, Christian Milani, Riccardo Mazza
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace report_moclog2;

require_once $CFG->libdir . '/formslib.php';

class ModulesForm extends \moodleform {

    protected function definition() {
        global $CFG;

        $form = $this->_form;

        

        /**
         * MOCLog2 reports
         */
        $form->addElement('header', 'moclog2_reports_header', "MOCLog2 reports");
        
        $form->addElement('checkbox', 'general_report', 'General report');
        $form->setType('general_report', PARAM_BOOL);
        $form->setDefault('general_report', true);
        
        
        
        
        
        $modules_types = ModulesManagement::getModulesTypes();
        
        /**
         * Activities
         */
        $form->addElement('header', 'resources', "Activities");
        foreach($modules_types[0] as $modulename){
            $module_id = ModulesManagement::getModuleIdByName($modulename);
            $id_checkbox = 'mod_' . $module_id;
            $form->addElement('checkbox', $id_checkbox, get_string('pluginname', 'mod_' . $modulename));
            $form->setType($id_checkbox, PARAM_BOOL);
            $form->setDefault($id_checkbox, true);
        }
        
        /**
         * Resources
         */
        $form->addElement('header', 'resources', "Resources");
        foreach($modules_types[1] as $modulename){
            $module_id = ModulesManagement::getModuleIdByName($modulename);
            
            $id_checkbox = 'mod_' . $module_id;
            $form->addElement('checkbox', $id_checkbox, get_string('pluginname', 'mod_' . $modulename));
            $form->setType($id_checkbox, PARAM_BOOL);
            $form->setDefault($id_checkbox, true);   
        }

        /**
         * Submit Button
         */
        $buttonarray = array();
        $buttonarray[] = $this->_form->createElement('submit', 'previous_button_modules', "Previous");
        $buttonarray[] = $this->_form->createElement('submit', 'next_button', "Next");
        $this->_form->addGroup($buttonarray, 'buttonar', '', array(' '), false);
        $this->_form->closeHeaderBefore('buttonar');
    }

    function validation($data, $files) {
        return array();
    }

}
