<?php

/**
 * MOCLog2 report
 *
 * @package    report_moclog2
 * @copyright  eLab Yann Cuttaz, Christian Milani, Riccardo Mazza
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace report_moclog2;

class CategoriesManagement {

    /**
     * Return an array with all sub categories id
     * @param type $parent
     */
    public static function getSubCategories($parentid) {
        global $DB;
        return $DB->get_records_sql("SELECT id FROM {course_categories} WHERE `path` LIKE '%/" . $parentid . "/%' OR path = '/" . $parentid . "'");
    }

    /**
     * Return the category from his id
     * @global \report_moclog2\type $DB
     * @param type $categoryid
     * @return type
     */
    public static function getCategoryByID($categoryid) {

        global $DB;
        $category = $DB->get_records_sql("SELECT * FROM {course_categories} WHERE id = '$categoryid'");
        return $category[key($category)];
    }

    /**
     * Get all categories ordered by path
     * @global \report_moclog2\type $DB
     * @return type
     */
    public static function GetCategoriesStructure() {
        global $DB;
        return $DB->get_records_sql("SELECT id, name, depth, path FROM {course_categories} ORDER BY path ASC");
    }

}
