<?php

/**
 * MOCLog2 report
 *
 * @package    report_moclog2
 * @copyright  eLab Yann Cuttaz, Christian Milani, Riccardo Mazza
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

if ($hassiteconfig) {
    $ADMIN->add('reports', new admin_externalpage('report_moclog2', get_string('pluginname', 'report_moclog2'), "$CFG->wwwroot/report/moclog2/index.php"));

    if ($ADMIN->fulltree) {
        /**
         * Colors
         */
        $settings->add(new admin_setting_heading('report_moclog2/header_colors', get_string('header_colors', 'report_moclog2'), null));
        $settings->add(new admin_setting_configcolourpicker('report_moclog2/progression_background_color', get_string('progression_background_color', 'report_moclog2'), null, null));
        $settings->add(new admin_setting_configcolourpicker('report_moclog2/progression_background_color_active', get_string('progression_background_color_active', 'report_moclog2'), null, null));
        $settings->add(new admin_setting_configcolourpicker('report_moclog2/progression_border_color', get_string('progression_border_color', 'report_moclog2'), null, null));
        
        
        
        /**
         * Set roles as $value=>$label array
         */
        $roles = report_moclog2\CronConfig::GetAllRoles();
        $choices = array();
        foreach ($roles as $role) {
            $choices[$role->id] = $role->shortname;
        }
        
        /**
         * Student roles
         */
        $settings->add(new admin_setting_heading('report_moclog2/header_role_student', get_string('header_roles', 'report_moclog2'), null));
        $settings->add(new admin_setting_configmulticheckbox('report_moclog2/students_role', get_string('role_students', 'report_moclog2'), null, null, $choices));
        
        /**
         * Teacher roles
         */
        $settings->add(new admin_setting_configmulticheckbox('report_moclog2/teachers_role', get_string('role_teachers', 'report_moclog2'), null, null, $choices));
        
    }
}