<?php

/**
 * MOCLog2 report
 *
 * @package    report_moclog2
 * @copyright  eLab Yann Cuttaz, Christian Milani, Riccardo Mazza
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require(dirname(__FILE__) . '/../../config.php');
require_once($CFG->libdir . '/adminlib.php');
admin_externalpage_setup('report_moclog2', '', null, '', array('pagelayout' => 'report'));

$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/report/moclog2/lib/moclog2/style/moclog2.css'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/report/moclog2/lib/moclog2/scripts/moclog2.js'));

/**
 * Create and show form
 */
$form = new \report_moclog2\ModulesForm();

if ($dataForm = $form->get_data()) {
    
    if(isset($dataForm->next_button)){
        
        /**
         * Load params object
         */
        $params = \report_moclog2\MocLog2Params::Load();

        /**
         * Save modules
         */
        $modules = array();
        foreach ($dataForm as $key => $value) {
            if (strpos($key, 'mod') !== false) {
                $modules[] = substr($key, 4);
            }
        }
        $params->modules_to_show = $modules;
        $params->general_report = isset($dataForm->general_report);

        /**
         * Save params object
         */
        $params->Save();

        redirect("select_params.php");
    } else{
        redirect("select_categories.php");
    }
} else {
    echo $OUTPUT->header();
    echo $OUTPUT->heading(get_string('pluginname', 'report_moclog2'));
    echo \report_moclog2\Progression::getCurrentProgression(1);
    $form->display();
}
echo $OUTPUT->footer();