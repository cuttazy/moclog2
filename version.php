<?php

/**
 * MOCLog2 report
 *
 * @package    report_moclog2
 * @copyright  eLab Yann Cuttaz, Christian Milani, Riccardo Mazza
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
 
$plugin->version   = 2016050301;
$plugin->requires  = 2014051200;
$plugin->cron      = 0;
$plugin->component = 'report_moclog2';
$plugin->maturity  = MATURITY_STABLE;
$plugin->release   = '1.0 (Build: 2016042500)';