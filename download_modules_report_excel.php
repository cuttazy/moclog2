<?php

/**
 * MOCLog2 report
 *
 * @package    report_moclog2
 * @copyright  eLab Yann Cuttaz, Christian Milani, Riccardo Mazza
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require(dirname(__FILE__).'/../../config.php');
require_once $CFG->dirroot.'/grade/export/lib.php';

$moduleid = required_param("moduleid", PARAM_INT);

if(is_siteadmin()){
    $params = \report_moclog2\MocLog2Params::Load(false);
    $moduleid = addslashes(strip_tags($moduleid));
    \report_moclog2\export_data::CreateModulesReportExcel($params, $moduleid);
}

