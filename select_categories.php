<?php

/**
 * MOCLog2 report
 *
 * @package    report_moclog2
 * @copyright  eLab Yann Cuttaz, Christian Milani, Riccardo Mazza
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require(dirname(__FILE__) . '/../../config.php');
require_once($CFG->libdir . '/adminlib.php');
admin_externalpage_setup('report_moclog2', '', null, '', array('pagelayout' => 'report'));

$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/report/moclog2/lib/moclog2/scripts/moclog2.js'));
$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/report/moclog2/lib/moclog2/style/moclog2.css'));

/**
 * Create and show form
 */
$form = new \report_moclog2\CategoriesForm();
if ($dataForm = $form->get_data()) {
    /**
     * Get data submitted
     */
    $params = new \report_moclog2\MocLog2Params();
    $params->categories = json_decode($dataForm->categories_to_show);
    $params->Save();

    /**
     * Redirect to next step
     */
    redirect("select_modules.php");
} else {
    echo $OUTPUT->header();
    echo $OUTPUT->heading(get_string('pluginname', 'report_moclog2'));
    
    echo \report_moclog2\Progression::getCurrentProgression(0);
    
    echo get_string('categoriesselectiondescription', 'report_moclog2');
    
    /**
     * Prepare data and show form
     */
    $categories = report_moclog2\CategoriesManagement::GetCategoriesStructure();

    echo '<br><br>';
    
    echo '<a href="#" onclick="selectAllCategories();">'. get_string('selectallcategories', 'report_moclog2') .'</a>';
    
    echo '<br><br>';

    foreach ($categories as $category) {
        echo '<div id="checkbox_parent_' . $category->id . '"><input type="checkbox" paramid="' . $category->id . '" parampath="' . $category->path . '" class="mclg2_box mclg2_box' . $category->depth . '" onclick="onClickCheckBox(this)">' . $category->name . '</div>';
    }
    $form->display();
}

echo $OUTPUT->footer();