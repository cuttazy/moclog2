<?php

/**
 * MOCLog2 report
 *
 * @package    report_moclog2
 * @copyright  eLab Yann Cuttaz, Christian Milani, Riccardo Mazza
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require(dirname(__FILE__) . '/../../config.php');
require_once($CFG->libdir . '/adminlib.php');
admin_externalpage_setup('report_moclog2', '', null, '', array('pagelayout' => 'report'));

/**
 * Delete all params of moclog2's session
 */
report_moclog2\MocLog2Params::destroy_session();

/**
 * If cron not runned, show a "warning" message, 
 * and the user can not use the plugin
 */
if (report_moclog2\MocLog2Params::cronRunned()){
    redirect("select_categories.php");
}
else {
    echo $OUTPUT->header();
    echo $OUTPUT->heading(get_string('pluginname', 'report_moclog2'));
    echo get_string("cronneverstart", "report_moclog2");
}
echo $OUTPUT->footer();




