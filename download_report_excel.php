<?php

/**
 * MOCLog2 report
 *
 * @package    report_moclog2
 * @copyright  eLab Yann Cuttaz, Christian Milani, Riccardo Mazza
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require(dirname(__FILE__) . '/../../config.php');
require_once $CFG->dirroot . '/grade/export/lib.php';

if (is_siteadmin()) {
    $params = \report_moclog2\MocLog2Params::Load(false);
    report_moclog2\export_data::CreateReportExcel($params);
}



