<?php

/**
 * MOCLog2 report EN translation file
 *
 * @package    report_moclog2
 * @copyright  eLab Yann Cuttaz, Christian Milani, Riccardo Mazza
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * General plugin strings
 */
$string['pluginname'] = 'MOCLog2';

$string['cron_title'] = 'MOCLog2 - task';
$string['cron_description'] = 'MOCLog2 - data exporter task';
$string['cronneverstart'] = "MOCLog2 task was never executed, please run MOCLog2 task (view README.txt) to export the data before using the reports.";

$string['categoriesselectiondescription'] = 'Choose which categories you want to see:';

$string['classespreferencesdescription'] = "MOCLog2 produces reports for each module available in Moodle. Each report will show the frequency distribution in classes. In this page you can set, add, and delete the classes for each module.";

$string['generalreportsdescription'] = "General reports containing overall statistics for the selected categories:";
$string['modulesreportsdescription'] = "Specific reports with frequency distribution for modules:";

/**
 * Params Form strings
 */
$string['hiddencourses'] = "Include Hidden Courses";
$string['hiddencourses_help'] = "Criteria for active courses";

$string['description_title'] = "Include only courses having the following criteria:";
$string['description_text'] = "";

$string['logsrequired'] = "Min number of logs";

$string['minenrolledstudents'] = "Min Enrolled Students";

$string['minenrolledteachers'] = "Min Enrolled Teachers";

$string['coursecreation'] = "Course Creation";
$string['coursecreation_help'] = "Include courses created after ...";

$string['logcreationstart'] = "Log creation start";
$string['logcreationstart_help'] = "Log from ...";

$string['logcreationend'] = "Log creation end";
$string['logcreationend_help'] = "Log to ...";

$string['selectallcategories'] = "Select all categories";

/**
 * Settings page
 */
$string['header_colors'] = "Colors";
$string['progression_background_color'] = "Progression background color";
$string['progression_background_color_active'] = "Progression background color active";
$string['progression_border_color'] = "Progression border color";
$string['header_roles'] = "Roles";
$string['role_teachers'] = "Teachers";
$string['role_students'] = "Students";

/**
 * Progression
 */
$string['progression_step1'] = "Select categories";
$string['progression_step2'] = "Select activities and resources";
$string['progression_step3'] = "Filtering";
$string['progression_step4'] = "Define classes";
$string['progression_step5'] = "Generate reports";

/**
 * Reports
 */
$string['report_numberofcourses'] = "Number of courses:";
$string['report_numberofemptycourses'] = "Number of empty courses:";
$string['report_averageofstudentspercourse'] = "Average of students per course:";
$string['report_averageofteachersspercourse'] = "Average of teachers per course:";
$string['report_averageofstudentslogperday'] = "Average of students’ log / day:";
$string['report_averageofteacherslogperday'] = "Average of teachers’ log / day:";
$string['report_averagevisibleresourcespercourse'] = "Average of visible resources per course:";
$string['report_averagehiddenresourcespercourse'] = "Average of hidden resources per course:";
$string['report_averagevisibleactivitiespercourse'] = "Average of visible activities per course:";
$string['report_averagehiddenactivitiespercourse'] = "Average of hidden activities per course:";
$string['report_averagemessagesforumpercourse'] = "Average messages of forum per course:";
$string['report_numberofassignmentpercourse'] = "Number of assignment submissions per course:";
$string['report_numberofassignmentperstudent'] = "Number of assignment submissions per student:";
$string['report_numberofchatmessagespercourse'] = "Number of chat messages per course:";
$string['report_numberofchatmessagesperstudent'] = "Number of chat messages per student:";
$string['report_numberofquizattempspercourse'] = "Number of quiz attemps per course:";
$string['report_numberofquizattempsperstudent'] = "Number of quiz attemps per student:";

$string['report_cronlastrun'] = "Cron last run: ";


/**
 * Module part of report
 * LIKE 'Average of %chat% per course'
 */
$string['report_averageof'] = "Average of ";
$string['report_percourse'] = " per course:";