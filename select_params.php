<?php

/**
 * MOCLog2 report
 *
 * @package    report_moclog2
 * @copyright  eLab Yann Cuttaz, Christian Milani, Riccardo Mazza
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require(dirname(__FILE__) . '/../../config.php');
require_once($CFG->libdir . '/adminlib.php');
admin_externalpage_setup('report_moclog2', '', null, '', array('pagelayout' => 'report'));

$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/report/moclog2/lib/moclog2/style/moclog2.css'));

$form = new \report_moclog2\SearchForm();

if ($form->is_cancelled()) {
    
} else if ($dataForm = $form->get_data()) {

    if(isset($dataForm->next_button_params)){
        /**
         * Load params object
         */
        $params = \report_moclog2\MocLog2Params::Load();

        if (!isset($dataForm->show_hidden_courses))
            $dataForm->show_hidden_courses = 0;

        /**
         * Add params to object
         */
        $params->hidden_courses = $dataForm->show_hidden_courses;
        $params->logs_required = $dataForm->logs_required;
        $params->min_enrolled_students = $dataForm->min_enrolled_students;
        $params->min_enrolled_teachers = $dataForm->min_enrolled_teachers;
        $params->log_creation_start = ($dataForm->log_creation_start == 0) ? \report_moclog2\CourseLog::getFirstLogDate() : $dataForm->log_creation_start;
        $params->log_creation_end = $dataForm->log_creation_end;
        $params->min_enrolled_teachers = $dataForm->min_enrolled_teachers;
        $params->course_creation = $dataForm->course_creation;

        $params->Save();

        /**
         * Redirect to next step
         */
        redirect("select_classes.php");
    } else{
        redirect("select_modules.php");
    }
}
else {
    echo $OUTPUT->header();
    echo $OUTPUT->heading(get_string('pluginname', 'report_moclog2'));

    echo \report_moclog2\Progression::getCurrentProgression(2);
    
    /**
     * Show params form
     */
    $form->display();
}
echo $OUTPUT->footer();
